/* Cette Classe permet de gérer la communication entre le client et le serveur.
 * En effet le serveur est bloqué dans l'attente de connexion d'autre client et
 * doit donc "déléguer" la gestion des communication a un Thread à part pour
 * gérer les communications.
 *
 * Cette Classe implément Runnable, mais pourrait tout aussi bien herité de
 * Thread.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;


public class conHandler implements Runnable {
	public Socket s = null;
	public Serveur iface = null;
	public int c ;
	public boolean online = true;
	String echoMessage=null;
	PrintWriter bP = null;
	BufferedReader bR = null;

    /* Le constructeur prend en parametre la socket créer par le serveur pour
     * communique avec un client particulier, le serveur (pour permettre entre
     * autre d'appeler la methode echo, un int representant le numéro du client
     * utile juste pour l'affichage, et les buffers d'ecriture et de lecture.
     *
     * Ce constructeur ne fait rien de spécial, en effet c'est le serveur qui
     * lance le Thread conHandler en appelant la methode start().
     */
	public conHandler(Socket soc, Serveur f, int cli, PrintWriter bP, BufferedReader bR){
		this.s = soc;
		this.iface = f;
		this.c = cli;
		this.bP = bP;
		this.bR= bR;
	}

    /* Comme conHandler implémente l'interface Runnable, il faut définir la
     * methode run.
     *
     * Cette methode run() est executer tant que le boleen online est vrai.
     *
     * Dans cette methode, conHandler lit le buffer de lecture. Cette lecture
     * est bloquante. Un peu à la manière d'un scanf en C.
     *
     * Quand un message est reçu (envoyé par le client auquel est associé le
     * Thread, celui ci appelle la fonction echo du serveur. (Pour information,
     * la fonction echo du serveur écrit sur tous les buffers ecriture associé
     * à chaque client pour que ceux-ci recoivent le message. La fonction echo
     * ecrit meme le message au client d'origne du message, d'ou le nom echo.
     *
     * il faut gérer la fermeture des buffers et des sockets quand le client
     * envoi la chaine de caractère "/quit" pour une fermeture propre.
     */
	public void run(){
        // Environ 30 lignes
	}
}

