## TP6 Chat  - 2016/2017 - RT-IUT.RE

Je donne ici une structure de code pour le TP Chat.

Vous pouvez vous servir de la nomenclature que j'ai donnée ou alors utilisé la votre

En bonus :

- Vous pouvez coté client, mettre des TextField permettant de donner l'adresse et le port pour se conencter au serveur et le pseudo utilisé.
- Vous pouvez afficher (coté client) la liste des usagers du chat.
- Vous pouvez gérer les messages destinés à une personne spécifique en la mettant le message en gras (type IRC)
- Vous pouvez gérer les messages privés qui ne s'afficheront que sur la fenetre du destinataire, et d'une autre couleur
- ...


