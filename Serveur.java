/* La classe Serveur.java est une classe qui permet de gérer les connexion des
 * client au chat. Son fonctionnement est très simple, il ouvre une socket
 * serveur et se met en attente de connexion client. Des qu'un client se
 * connecte il crée une instance de la classe conHandler (qui est un Thread) et
 * se remet en attente d'une autre connexion.
 *
 * C'est aussi le serveur qui grace à sa methode echo permet d'envoyer une
 * copie d'un message à tous les utilisateurs du chat.
 * 
 * This code is under MIT Licence.
 *  
 */
import java.util.ArrayList;
import java.net.*;
import java.io.*;


public class Serveur {
	Serveur serveurInterface = null;
	ServerSocket sServeur = null;

	ArrayList<Socket> sSocket = new ArrayList<>();
	ArrayList<conHandler> conn = new ArrayList<>();

	ArrayList<BufferedReader> bRead = new ArrayList<>();
	ArrayList<PrintWriter> bPrint = new ArrayList<>();

	String echoMessage=null;
	int nbClient = 0;

    /* Le constructeur de serveur créer une socket server sur le port 9000 et
     * se met en attente de connexion de client. Il faut dans tous les acs
     * lancer le serveur avant le client.
     *
     * Une fois la socket serveur ouverte, le serveur se met en attente de
     * connexion client. Il faut noter que cette attente est bloquante (un peu
     * comme un scanf en C. Tant qu'un client ne se connecte pas, le serveur
     * reste en attente sur cette ligne de code.
     *
     * Des qu'un client se cconnecte, le serveur lui cree une socket, associe
     * un buffer en lecture et un buffer en ecriture et creer une instance de
     * la classe conHandler qui va gérer ces sockets et buffers dans une
     * Thread. Le serveur demarre ce Thread.
     *
     */
	public Serveur() {
        // environ 25 lignes
	}

    /* La fonction echo, permet d'ecrire un message envoyé par un client à tous
     * les autres clients de la conversation. C'est l'instance de conHandler
     * qui appelle cette fonction
     */
    public synchronized void echo(String msg){
        // environ 3 lignes
	}

	public static void main(String[] args) {
		new Serveur();
	}

}
