/* Interface client
 * Cette classe herite de JFrame et implement Runnable et ActionListener
 * On contruit la fenetre de  conversation avec TextArea, un TextField
 * un bouton et un  champ pour le pseudo. On fait ent sorte que l'on puisse
 * avoir une barre de défilement.
 *
 * La construction de cette interface se fait dans le constructeur et à la
 * connexion au serveur se fait aussi dans le constructeur.
 *
 * A l'etablissement de la connection avec le serveur, le client lance en
 * tache de fond un Thread qui lui permet d'ecouter/lire sur la socket ce que
 * le serveur lui envoit.
 * Il lance aussi  une methode écrire ouvrant la socket en ecriture.
 * L'écriture en elle meme ne se fait qu'a lui suite d'un clique sur le bouton
 * envoyer.
 *
 * */
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Client extends JFrame implements Runnable, ActionListener {

    public boolean online = true;
	JFrame chatWindow = null;

	JButton btnSend = null;
	JTextField txtToSend = null;
	JTextArea txtArea = null;
	JTextField nameArea = null;

	JPanel chatArea = null;
	JPanel chatTxtBtn = null;

	JScrollPane scroll = null;

	Socket sSocket = null;
	BufferedReader bRead = null;
	PrintWriter bPrint = null;

    /* Construteur de la Classe. Ici, on dessine l'application graphique. Dans
     * laquelle on met un TextArea, un JButton, un TextField pour le pseudo et
     * un autre TextField pour le text a envoyer. Le TextArea est "mit" dans un
     * JScrollPane pour permettre d'avoir une barre de défilement.
     *
     * Une fois l'interface graphique construite , on crée une socket qu'on
     * connecte au serveur.
     *
     * Ensuite, on lance les methodes lire() et ecrire()
     */
	public Client(){
        // environ 40 lignes
	}

    /* La methode ecrire() instancie simplement PrintWriter pour pouvoir ecrire
     * sur la  socket en ecriture.
     */
	public void ecrire(){
        // Environ 5 lignes
	}

    /*
     * La methode lire() lance le Thread client avec la methode start.
     */
	public void lire(){
        // Une ligne
	}


	public static void main(String[] args) {
		new Client();
	}

    /* La methode Run doit etre implémentée car Client implémente Runnable. Qui
     * est un autre moyen d'utiliser les Thread sans passer par un héritage.
     * Impossible dans notre cas car Client herite deja de JFrame.
     *
     * Ici dans une boucle infinie, le Thread lie ce qu'il y a dans la socket,
     * et  met le contenu de la socket dans le TextArea.
     *
     * En cas d'erreur ou à la fin, ne pas oublier de fermer les buffers
     * (lecture et ecriture et de fermer la socket).
     * Ici il faut aussi gérer le cas ou le serveur se ferme. Dans ce cas le
     * message reçu sur le buffer est "null". Il faut gérer ce cas pour uen
     * fermeture propre.
     */
	public void run() {
        // Environ 20 lignes
	}

    /* L'aciont lié au bouton et à l'envoi d'un message au serveur avec le
     * pseudo avant le message lui meme.
     *
     * Ici, je gère aussi la command de chat /quit qui permet de mettre fin au
     * thread et d'annoncer au serveur que l'on quitte la discussion pour que
     * le serveur puisse fermer correctement les sockets.
     */
	public void actionPerformed(ActionEvent arg0) {
        // Environ 10 lignes
	}
}
